﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using QuizMVC.Models;

namespace QuizMVC.Bl.Services
{
    public class GameService : IGameService
    {
        Random random = new Random();
        TVHost TVHost = new TVHost();
        Player player = new Player();

        /// <summary>
        /// Начало игры
        /// </summary>
        /// <param name="itirations"></param>
        /// <returns></returns>
        public GameResult StartGame(int itirations)
        {
            var gameResult = new GameResult();

            for (int i = 0; i < itirations; i++)
            {
                var doors = CreateВoors();
                var doorPlayerChoose = player.SelectDoor(doors, random);

                TVHost.OpenDoor(doors);

                var Win = player.OpenDoor(doorPlayerChoose);

                if (Win)
                    gameResult.DontAgree++;
            }

            for (int i = 0; i < itirations; i++)
            {
                var doors = CreateВoors();
                var doorPlayerChoose = player.SelectDoor(doors, random);

                TVHost.OpenDoor(doors);

                var Win = player.SwitchDoor(doors);

                if (Win)
                    gameResult.Agreed++;
            }

            СalculationChance(gameResult, itirations);

            return gameResult;
        }

        /// <summary>
        /// Подсчитываем вероятность
        /// </summary>
        private void СalculationChance(GameResult gameResult, int itirations)
        {
            gameResult.AgreedChance = ((float)gameResult.Agreed / itirations) * 100;
            gameResult.DontAgreeChance = ((float)gameResult.DontAgree / itirations) * 100;
        }

        /// <summary>
        /// Генерируем двери
        /// </summary>
        /// <returns></returns>
        List<Door> CreateВoors()
        {
            var doors = new List<Door>()
                {
                    new Door(),
                    new Door(),
                    new Door()
                };
            var carBehindDoor = random.Next(0, 3);
            doors[carBehindDoor].Prize = true;
            return doors;
        }
    }
}