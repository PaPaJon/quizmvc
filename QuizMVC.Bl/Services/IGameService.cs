﻿using QuizMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizMVC.Bl.Services
{
    public interface IGameService
    {
        GameResult StartGame(int itiration);
    }
}
