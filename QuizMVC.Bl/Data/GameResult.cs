﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizMVC.Models
{
    /// <summary>
    /// Результат игры
    /// </summary>
    public class GameResult
    {

        public int Agreed { get; set; }
        public float AgreedChance { get; set; }
        public int DontAgree { get; set; }
        public float DontAgreeChance { get; set; }
    }
}