﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;

namespace QuizMVC.Models
{
    /// <summary>
    /// Игрок
    /// </summary>
    public class Player : Human
    {
        /// <summary>
        /// Игрок выбирает одну из трейх дверей
        /// </summary>
        /// <param name="doors"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public Door SelectDoor(IList<Door> doors, Random random)
        {
            Thread.Sleep(10);
            var selectedDoor = random.Next(0, 3);
            doors[selectedDoor].DoorPlayerChoosed = true;
            return doors[selectedDoor];
        }

        /// <summary>
        /// Игрок открывает дверь
        /// </summary>
        /// <param name="door"></param>
        /// <returns></returns>
        public bool OpenDoor(Door door)
        {
            door.Open = true;
            return door.Prize;
        }

        /// <summary>
        /// Игорок меняет дверь
        /// </summary>
        /// <param name="doors"></param>
        /// <returns></returns>
        internal bool SwitchDoor(IList<Door> doors)
        {
            var door = doors.FirstOrDefault(x => (x.DoorPlayerChoosed == false) && (x.Open == false));
            return door.Prize;
        }
    }
}