﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizMVC.Models
{
    /// <summary>
    /// Дверь
    /// </summary>
    public class Door
    {
        public bool Prize { get; set; }
        public bool Open { get; set; }
        public bool DoorPlayerChoosed { get; set; }
    }
}