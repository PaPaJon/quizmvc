﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizMVC.Models
{
    /// <summary>
    /// Телеведущий
    /// </summary>
    public class TVHost : Human
    {
        /// <summary>
        /// Ведущий открывате дверь с козой
        /// </summary>
        /// <param name="doors"></param>
        public void OpenDoor(IList<Door> doors)
        {
            doors.FirstOrDefault(x => (x.DoorPlayerChoosed == false) && (x.Prize == false)).Open = true;
        }
    }
}