﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuizMVC.Models
{
    public class GameResultViewModel
    {
        public string AgreedChance { get; set; }
        public string DontAgreeChance { get; set; }
    }
}