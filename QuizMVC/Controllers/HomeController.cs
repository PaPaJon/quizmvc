﻿using QuizMVC.Bl.Services;
using QuizMVC.Models;
using System.Web.Mvc;

namespace QuizMVC.Controllers
{
    public class HomeController : Controller
    {
        IGameService _gameService;

        public HomeController()
        {
            _gameService = new GameService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult StartGame(int itirations)
        {
            var result = _gameService.StartGame(itirations);
            var gameResult = new GameResultViewModel()
            {
                AgreedChance = $"Шанс при согласии смены выбора: {result.AgreedChance}",
                DontAgreeChance = $"Шанс при отказе смены выбора: {result.DontAgreeChance}"
            };

            return View(gameResult);
        }
    }
}